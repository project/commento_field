<?php

namespace Drupal\commento_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'commento' widget.
 *
 * @FieldWidget(
 *   id = "commento",
 *   module = "commento_field",
 *   label = @Translation("Commento"),
 *   field_types = {
 *     "commento"
 *   }
 * )
 */
class CommentoWidget extends WidgetBase {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, AccountInterface $current_user) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, []);
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Commento Comments'),
      '#description' => $this->t('Users can post comments using <a href=":commento">Commento</a>.', [':commento' => 'https://commento.io/']),
      '#default_value' => isset($items->status) ? $items->status : TRUE,
      '#access' => $this->currentUser->hasPermission('toggle commento comments'),
    ];

    return $element;
  }

}
