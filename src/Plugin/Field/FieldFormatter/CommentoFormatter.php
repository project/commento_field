<?php

namespace Drupal\commento_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'commento' formatter.
 *
 * @FieldFormatter(
 *   id = "commento",
 *   label = @Translation("Commento"),
 *   field_types = {
 *     "commento"
 *   }
 * )
 */
class CommentoFormatter extends FormatterBase {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructs a new CommentoFormatter.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    AccountInterface $current_user
  ) {
    parent::__construct(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $label,
      $view_mode,
      []
    );
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'css_override' => '',
      'disable_auto_init' => FALSE,
      'disable_default_font' => FALSE,
      'hide_deleted_comments' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);
    $elements['css_override'] = [
      '#type' => 'url',
      '#title' => $this->t('CSS override URL'),
      '#default_value' => $this->getSetting('css_override'),
      '#description' => $this->t(
        "An URL to a CSS file with overriding styles. Defaults to no override and uses Commento's default theme."
      ),
    ];
    $elements['disable_auto_init'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable auto init'),
      '#default_value' => $this->getSetting('disable_auto_init'),
      '#description' => $this->t(
        'Commento automatically initialises itself when the page is loaded. If you prefer to load Commento dynamically (for example, after the user clicks a button), you can disable this.'
      ),
    ];
    $elements['disable_default_font'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable default font'),
      '#default_value' => $this->getSetting('disable_default_font'),
      '#description' => $this->t(
        "By default, Commento uses the Source Sans Pro font to present a good design out-of-the-box. If you'd like to disable this so that Commento never loads the font files, you can set this to true."
      ),
    ];
    $elements['hide_deleted_comments'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide deleted comments'),
      '#default_value' => $this->getSetting('hide_deleted_comments'),
      '#description' => $this->t(
        "By default, deleted comments with undeleted replies are shown with a \"[deleted]\" tag. If you'd like to disable this, setting this to true will hide deleted comments even if there are legitimate replies underneath. Deleted comments without any undeleted comments underneath are hidden irrespective of the value of this function"
      ),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    // As the Field API only applies the "field default value" to newly created
    // entities, we'll apply the default value for existing entities.
    if ($items->count() == 0) {
      $field_default_value = $items->getFieldDefinition()->getDefaultValue(
        $items->getEntity()
      );
      $items->status = $field_default_value[0]['status'];
    }

    if (!$items->status || !$this->currentUser->hasPermission('view commento comments')) {
      return $element;
    }

    $settings = $this->getSettings();

    $element[] = [
      '#type' => 'commento',
      '#url' => $items->getEntity()->toUrl('canonical')->toString(),
      '#css_override' => $settings['css_override'],
      '#disable_auto_init' => (boolean) $settings['disable_auto_init'],
      '#disable_default_font' => (boolean) $settings['disable_default_font'],
      '#hide_deleted_comments' => (boolean) $settings['hide_deleted_comments'],
    ];

    return $element;
  }

}
