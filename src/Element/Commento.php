<?php

namespace Drupal\commento_field\Element;

use Drupal\Core\Render\Element\RenderElement;

/**
 * Provides Commento render element.
 *
 * @RenderElement("commento")
 */
class Commento extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#title' => '',
      '#url' => '',
      '#identifier' => '',
      '#callbacks' => '',
      '#attributes' => ['id' => 'commento_thread'],
      '#pre_render' => [
        [$class, 'generatePlaceholder'],
      ],
    ];
  }

  /**
   * Pre_render callback to generate a placeholder.
   *
   * @param array $element
   *   A renderable array.
   *
   * @return array
   *   The updated renderable array containing the placeholder.
   */
  public static function generatePlaceholder(array $element) {
    $account = \Drupal::currentUser();
    if (!$account->hasPermission('view commento comments')) {
      return $element;
    }

    $element['script'] = [
      '#type' => 'html_tag',
      '#tag' => 'script',
      '#value' => '',
      '#attributes' => [
        'src' => 'https://cdn.commento.io/js/commento.js',
        'defer' => TRUE,
        'data-auto-init' => var_export(!$element['#disable_auto_init'], TRUE),
        'data-no-fonts' => var_export($element['#disable_default_font'], TRUE),
        'data-hide-deleted' => var_export(
          $element['#hide_deleted_comments'],
          TRUE
        ),
        'data-page-id' => $element['#url'],
      ],
    ];

    $element['container'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#value' => '',
      '#attributes' => [
        'id' => 'commento',
      ],
    ];

    $element['noscript'] = [
      '#type' => 'html_tag',
      '#tag' => 'noscript',
      '#value' => t('Please enable JavaScript to load the comments.'),
    ];

    return $element;
  }

}
